from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjetcForm


@login_required
def list_project(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects_list": projects,
    }

    return render(request, "projects/list.html", context)


@login_required
def detail_list(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "projects_object": project,
    }

    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjetcForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("list_projects")
    else:
        form = ProjetcForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
