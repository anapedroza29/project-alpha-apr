from django.urls import path
from projects.views import list_project, detail_list, create_project

urlpatterns = [
    path("", list_project, name="list_projects"),
    path("<int:id>/", detail_list, name="show_project"),
    path("create/", create_project, name="create_project"),
]
